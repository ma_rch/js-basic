const inputData = {
    B: [1, 15],
    I: [16, 30],
    N: [31, 45],
    G: [46, 60],
    O: [61, 75],
};

function getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function generateArray(char, min, max) {
    const array = [];
    const flag = char === "N";
    let iterations = flag ? 4 : 5;

    for (let i = 0; i < iterations; i += 1) {
        let cell = "";
        while (array.includes(cell) || cell === "") {
            cell = char + getRandomNumber(min, max);
        }
        array.push(cell);
    }
    return array;
}

function getCard() {
    const result = Object.entries(inputData).reduce(
        (accumulator, [key, val]) => {
            const [min, max] = val;
            accumulator.push(generateArray(key, min, max));
            return accumulator;
        },
        []
    );

    return result.flat();
}

console.log(getCard(inputData));
