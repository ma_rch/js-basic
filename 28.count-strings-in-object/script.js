function strCount(obj, count = 0) {
    if (typeof obj === "string") {
        count += 1;
    } else if ((typeof obj === "object") & (obj !== null)) {
        for (const key in obj) {
            count = strCount(obj[key], count);
        }
    }
    return count;
}

const exampleObject = {
    first: "1",
    second: "2",
    third: false,
    fourth: ["and another", 2, "and again", [undefined, "and again"], false],
    fifth: null,
};

const result = strCount(exampleObject);
console.log(result); // Output will be 5
